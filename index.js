/*************************************
 * DINOVA GET ALL RESTAURANTS EXAMPLE
 * ----------------------------------
 * Usage:
 * 
 * 	GET ALL
 * 		node index.js
 * 
 * 	GET UPDATED
 * 		node index.js <dateString>
 * 		
 * 		node index.js 2020-05-01
 * 
 *************************************/

/***********************
 * PREREQUISITES
 ***********************/ 
const FULL_ACCESS_KEY	= ""; // <-- SET YOUR FULL ACCESS API KEY AS PROVIDED HERE

/***********************
 * REQUIRES
 ***********************/
const axios				= require('axios');

/***********************
 * CONSTANTS
 ***********************/
const BASE_URL			= "https://dev.dinova.com/api-restaurant/v1/restaurants/search";
const DEFAULT_PARAMS	= {
	where : {},
	size : 500
};

// INIT DEFAULTS
axios.defaults.headers.common['x-api-key'] = FULL_ACCESS_KEY;

/***********************
 * FUNCTIONS
 ***********************/
async function getAll(params, restaurants)
{
	restaurants = restaurants || [];

	try
	{
		// perform request
		const response = await axios.get(BASE_URL, {params : params});
		
		// push results into array
		restaurants.push(...response.data.restaurants);

		// log current progress
		console.log(`Retrieved ${response.data.page.start}-${response.data.page.end} of ${response.data.page.total}`);

		if(response.data.page.nextPageToken)
		{
			params.pageToken = response.data.page.nextPageToken;
			await getAll(params, restaurants);
		} 
		
		return restaurants;
	}catch(e)
	{
		if(e && e.response && e.response.data)
			console.log(e.response.data);
		else
			console.log(e);
	}	
}

async function getAllRestaurants()
{
	console.log("\nGetting all restaurants...\n");

	const restaurants = await getAll(DEFAULT_PARAMS);

	console.log(`Finished Get All Restaurants - retrieved ${restaurants ? restaurants.length : 0} restaurants\n`);
}

async function getUpdatedRestaurants(dateString)
{
	console.log(`\nGetting updated restaurants since ${dateString} ...\n`);

	const restaurants = await getAll({...DEFAULT_PARAMS, ...{lastUpdate : dateString}});

	console.log(`Finished Get Updated Restaurants since ${dateString} - retrieved ${restaurants ? restaurants.length : 0} restaurants\n`);
} 

/***********************
 * RUN NOW
 ***********************/
if(!FULL_ACCESS_KEY)
{
	console.log("\nPlease set the FULL_ACCESS_KEY in the index.js file before using this example\n");
}else
{
	if(process.argv.length > 2)
		getUpdatedRestaurants(process.argv[2]);
	else
		getAllRestaurants();
}
